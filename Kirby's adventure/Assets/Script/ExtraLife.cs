﻿using UnityEngine;
using System.Collections;

// Add components when Script is added to GameObject
// - Components must still be edited/modified after being added
[RequireComponent(typeof(Rigidbody2D))]		// Rigidbody2D is added if one is not already present
[RequireComponent(typeof(BoxCollider2D))]	// BoxCollider2D is added if one is not already present



public class ExtraLife : MonoBehaviour
{

    // Example of calling method in Start...

    // Use this for initialization
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {

    }

    // Checks for Collisions
    // - At least one GameObject must have a Rigidbody2D attached
    // - Will not work if Collider is set to "Is Trigger"
    void OnCollisionEnter2D(Collision2D c)
    {
        //Debug.Log (c.gameObject.name);
        //Debug.Log (c.gameObject.tag);

        // Check if GameObject tagged as Player hit the Collectible
        if (c.gameObject.tag == "Player")
        {
            // Player hit Collectible, do something
            // Add one life to Player
            CharacterController_Mover cm = c.gameObject.GetComponent<CharacterController_Mover>();
            if (cm)
            {
                cm.lives++;
            }

            // Destroy Collectible GameObject
            Destroy(gameObject);
        }
    }
}
