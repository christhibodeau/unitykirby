﻿using UnityEngine;
using System.Collections;

public class CharacterController_Mover : MonoBehaviour 
{

    
    //Mehtod 2: reference Rigidbody through inspector
    public Rigidbody2D rb2;

	//Handle movement speed of character
    //can be adjusted through inspector while in play mode
    public float speed;

    //handles jump speed for character
    public float jumpForce;
    public bool isGrounded;
    public LayerMask isGroundLayer;
    public Transform groundCheck;

    //public bool shoot;
    //public bool shoot2;
    //public bool slide;

    //animation transition
    Animator anim;

    //character flippage
    public bool isFacingRight;

    
    // Used to keep track of Player lives
    int _lives = 3;
    
    // Use this for initialization
	void Start () 
    {
    
        if (!rb2)
        {
            Debug.LogError("No Rigidbody2D found on Character.");
        }

        //speed check, if 0 it'll set to 5. 
        if(speed == 0)
        {
            Debug.LogWarning("speed not set in inspector. Defaulting to 5.");
            speed = 5.0f;
        }

        if (jumpForce == 0)
        {
            
            Debug.LogWarning("jumpForce not set in inspector. Defaulting to 5.");
            jumpForce = 7.0f;
        } 
        
        anim = GetComponent<Animator>();
        if(!anim)
        {
            Debug.LogWarning("Animator F'ed up man.");

        }

    }
	
	// Update is called once per frame
	void Update () 
    {
	   isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, isGroundLayer);
             
       float moveValue = Input.GetAxisRaw("Horizontal");
        
       Debug.Log(moveValue);

       //allows for jump option when touching ground.
       if(Input.GetButtonDown("Jump") /*&& isGrounded*/)
       {
           Debug.Log("Jump");
           
           rb2.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
       }
       rb2.velocity = new Vector2(moveValue * speed, rb2.velocity.y);

       //slide when moving and down is pressed
    /*   if (Input.GetKey(KeyCode.DownArrow) && isGrounded && Mathf.Abs(moveValue) > 0.11f)
       {
           anim.SetBool("Slide", slide = true);
       }
       else anim.SetBool("Slide", slide = false);

       // basic shooting animation
       if (Input.GetButtonDown("Fire1"))
       {
           anim.SetBool("Fire", shoot = true);
       }
       else anim.SetBool("Fire", shoot = false);

       
       //running shooting animation
       if (Input.GetButtonDown("Fire1") && isGrounded && Mathf.Abs(moveValue) > 0.11f)
       {
           anim.SetBool("Runshoot", shoot2 = true);
       }
       else anim.SetBool("Runshoot", shoot2 = false);
       */
       //speed for movement
       anim.SetFloat("Speed", Mathf.Abs(moveValue));

       //grounded bool for jumping
       anim.SetBool("Grounded", isGrounded);      

       if ((isFacingRight && moveValue < 0) || (!isFacingRight && moveValue > 0))
       {
          flipCharacter ();
       }
       
    }

    void flipCharacter()
    {
        //toggle isFacingRight variable
        isFacingRight = !isFacingRight;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;

    }

    // Give access to private data (variables)
    // Not needed if using public access type
    public int lives	// int lives matches variable declaration minus "_" before name
    {
        get
        {
            // Return private variable value
            return _lives;
        }

        set
        {
            // Update score by a value
            _lives = value;

            Debug.Log("Lives changed to " + _lives);
        }
    }
}

